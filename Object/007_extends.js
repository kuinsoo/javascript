class Person{
   constructor(name,first,second){
      this.name=name;
      this.first=first;
      this.second=second;
   }
   sum(){
      return this.first+this.second;
   }
}

// let kim = new Persopn('kim', 10, 20);
// console.log(kim);


class PersonPlus{ // 상속 받기 전 수정 사항
   constructor(name,first,second){
      this.name=name;
      this.first=first;
      this.second=second;
   }
   sum(){
      return this.first+this.second;
   }
   avg(){
      return this.sum()/2;
   }
}

class PersonInheritance extends PersonPlus{ //상속 받았을때
   avg(){
      return this.sum()/2;
   }
   div(){
      return this.first/this.second;
   }
}

let kim = new PersonInheritance('kim', 10, 20);

console.log(kim.sum());
console.log(kim.avg());
console.log(kim.div());
