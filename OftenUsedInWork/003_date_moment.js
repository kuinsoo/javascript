/* 날짜 */
const moment = require('moment')

const today = moment().format('YYYY년 MM월 DD일')
console.log(today) // 2021년 07월 29일
console.log('today - us ', moment().format('MMM D, YYYY')) // today - us  Jul 29, 2021 

console.log('+1 day', moment().add(1, 'day').format('YYY년 MM월 DD일'));
console.log('+1 day', moment().add(-1, 'day').format('YYY년 MM월 DD일'));
console.log('+1 day', moment().subtract(1, 'day').format('YYY년 MM월 DD일'));
console.log('+1 day', moment().subtract(3, 'day').format('YYY년 MM월 DD일'));

console.log('+1 week', moment().add(1, 'week').format('YYYY MM DD'));
console.log('-1 week', moment().subtract(1, 'week').format('YYYY MM DD'));

console.log('+1 month', moment().add(1, 'month').format('YYYY MM DD'));
console.log('-1 month', moment().subtract(1, 'month').format('YYYY MM DD'));

console.log('+1 month', moment().add(1, 'year').format('YYYY MM DD'));
console.log('-1 month', moment().subtract(1, 'year').format('YYYY MM DD'));

console.log(moment.locale('en'));
const creationDate = moment().utc(); // UTC+0
console.log(creationDate);
const userTimezone = 9

console.log(moment(creationDate).format('YYYY-MM-DD HH:mm:ss'));
console.log(moment(creationDate).add(userTimezone, 'hour').format('YYYY-MM-DD HH:mm'));
