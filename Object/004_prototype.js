// // 3. constructor 함수 생성
// function Person(name, first, second, third) {
//   this.name=name;
//   this.first=first;
//   this.second=second;
//   this.third=third;
//   this.sum = function() { // 함수가 생성될때마다 실행되서 메모리 소비 *1>
//     return this.first+this.second+this.third;
//   }
// }

// console.log('Person()', Person());
// console.log('new Person()', new Person('person', 10,10,10)); // 생성자 호출

// // 4. constructor 함수 사용방법
// var kim = new Person('kim',10,20,30);
// kim.sum = function() { // <1* 이와 같은 작업을 반복적으로 만들어야한다.  
//   return 'modified : ' + (this.first+this.second);
// }
// var lee = new Person('lee',10,10,10);
// console.log("kim.sum()", kim.sum());
// console.log("lee.sum()", lee.sum());


console.log('====================================');
console.log("공통적인 사용을 위해 수정");
console.log('====================================');


// 3. constructor 함수 생성
function Person(name, first, second, third) {
  this.name=name;
  this.first=first;
  this.second=second;
  this.third=third;
}

// 한번만 정의 되기 때문에 메모리 낭비 방지와 성능 향상에 도움
Person.prototype.sum = function() {
  return "prototype : " + (this.first+this.second);
}

console.log('Person()', Person());
console.log('new Person()', new Person('person', 10,10,10)); // 생성자 호출

// 4. constructor 함수 사용방법
var kim = new Person('kim',10,20,30);
kim.sum = function() { // 따로 정의 하여 사용하기 위해서 재설정 
  return 'this : ' + (this.first+this.second);
}
var lee = new Person('lee',10,10,10); // prototype 을 찾아서 실행
console.log("kim.sum()", kim.sum());
console.log("lee.sum()", lee.sum());

