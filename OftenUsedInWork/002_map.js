/* filter 응용 */
const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present']
const result = words.filter(word => word.length > 6)
console.log(result);
console.log(words.filter(word => word.match('spray')))

/* Map 응용 */
const userList = [
    {id:1, fullName:'kkuinsoo', phone:'01099470728'},
    {id:2, fullName:'miso', phone:'01099470202'},
]

console.log(userList);

console.log(userList[0].phone.substr(0,3) + '-' + userList[0].phone.substr(3,4) + '-' + userList[0].phone.substr(7,4))
const newUserList = userList.map(user => (
    {
        id: user.id,
        fullName: user.fullName,
        phone: userList[0].phone.substr(0,3) + '-' + userList[0].phone.substr(3,4) + '-' + userList[0].phone.substr(7,4)
    })
)

console.log(newUserList);

var a = "a";
var b = "a";
if (a < b) { // true
  console.log(a + " is less than " + b);
} else if (a > b) {
  console.log(a + " is greater than " + b);
} else {
  console.log(a + " and " + b + " are equal.");
}


const s_prim = 'foo'
const s_obj = new String('foo')
console.log(s_prim);
console.log(s_obj.toString());
if(s_prim === s_obj) {
    console.log(s_prim == s_obj);
}else {
    console.log(s_prim === s_obj);
}

let s1 = '2 + 2'
let s2 = new String('2 + 2')
console.log(eval(s1));
console.log(eval(s2.valueOf()));

