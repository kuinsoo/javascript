/*************************************************************
최종수정일:

일반함수

btMov_AddList
checkNumber

일반함수

BT_getComboData 셀렉트 생성
BT_CheckLastAcctno 계좌번호 끝에 3자리 000 제거
BT_makeAcntnoDash 계좌번호에 하이픈을 넣음
BT_MakeAcntList 계좌번호를 리스트로 만듬
BT_MakePayAcntList 계좌번호를 리스트로 만듬
BT_ReduceDash 하이픈을 삭제
BT_makeCardnoDash 카드번호 하이픈 넣기
BT_getToday 오늘 날짜열을 YYYYMMDD의 형식으로 만듬
BT_make2Length 월일을 두자리로 만듬

BT_makeCardnoDash 카드번호 하이픈 넣음
BT_makeMembernoDash 회원번호(16자리) 하이픈 넣기

BT_TrimSpace 공백없애기

BT_calc 금액 더하기 계산
BT_Del_comma 콤마 삭제
BT_Add_comma 입력폼 작성시 콤마 추가
BTCom_Replace 문자변환

BT_Add_Displaycomma 금액 Display 시 콤마 추가
BT_Add_NumberComma 금액의 앞자리 0을 없애고 콤마추가
BT_Add_NumberComma1
BT_toMoneyF 환율,외화에 소수점,콤마 추가

BT_Reduce_comma 콤마 제거

BT_isAlphabet 값이 알파벳만 있는지 체크
BT_isNumber 값이 숫자만 있는지 체크
BT_number 숫자만 입력
BT_isTrsfPass 이체 비밀번호 체크 루틴

BT_getRandomNumber 씨크릿 카드 번호 생성

BT_makeCustnoDash 9자리 고객번호 하이픈을 넣음

BT_makeResnoDash 주민번호 하이픈 넣기

BT_makeLoannoDash 대출계좌에 하이픈 넣기

BT_makeDateDash yyyy-mm-dd
BT_makeDateDash_1 yyyy-mm
BT_makeDateDash_2 mm-dd
BT_makeDateDash_3 mm
BT_makeTime 00:00:00

BT_StrToInt 문자열을 10진수로 변환

BT_makeDecimalPoint 소수점이 포함된 숫자 앞에 포함된 0 을 제거 

last_page 페이지 설정

BT_EngNum 숫자와영어만 허용

BT_SetServiceCode ServiceCode박아주기
BT_Setreturn_url return_url 박아주기 
BT_getRandomNumber() { //씨크릿 카드 순차번호 생성

pwCheck() 비밀번호 연번 확인 => 주석확인

***************************************************************/
var alphaArray = new Array();
alphaArray[0]  = 'a' ;alphaArray[1] = 'b';alphaArray[2] = 'c';alphaArray[3] = 'd';
alphaArray[4]  = 'e' ;alphaArray[5] = 'f';alphaArray[6] = 'g';alphaArray[7] = 'h';
alphaArray[8]  = 'i' ;alphaArray[9] = 'j';alphaArray[10] = 'k';alphaArray[11] = 'l';
alphaArray[12] = 'm';alphaArray[13] = 'n';alphaArray[14] = 'o';alphaArray[15] = 'p';
alphaArray[16] = 'q';alphaArray[17] = 'r';alphaArray[18] = 's';alphaArray[19] = 't';
alphaArray[20] = 'u';alphaArray[21] = 'v';alphaArray[22] = 'w';alphaArray[23] = 'x';
alphaArray[24] = 'y';alphaArray[25] = 'z';
alphaArray[26] = 'A';alphaArray[27] = 'B';alphaArray[28] = 'C';alphaArray[29] = 'D';
alphaArray[30] = 'E';alphaArray[31] = 'F';alphaArray[32] = 'G';alphaArray[33] = 'H';
alphaArray[34] = 'I';alphaArray[35] = 'J';alphaArray[36] = 'K';alphaArray[37] = 'L';
alphaArray[38] = 'M';alphaArray[39] = 'N';alphaArray[40] = 'O';alphaArray[41] = 'P';
alphaArray[42] = 'Q';alphaArray[43] = 'R';alphaArray[44] = 'S';alphaArray[45] = 'T';
alphaArray[46] = 'U';alphaArray[47] = 'V';alphaArray[48] = 'W';alphaArray[49] = 'X';
alphaArray[50] = 'Y';alphaArray[51] = 'Z';

function getCode(argObj1,argObj2){
	Key1 = argObj1;
	Key2 = argObj2;
	if(top.iWallet.LoadCode_state == false){
		top.iWallet.LoadCode = top.iWallet.LoadCode + 1;
		if(top.iWallet.LoadCode <= 5){
			setTimeout("getCode(Key1,Key2)", 1000)
		}else{
			alert("코드 초기화에 실패하였습니다.\n처음으로 돌아갑니다.")
			SID = get_sessionid();
			CryptoClient.Logout(SID);	
			top.location.href="/btindex.asp";
		}
	}else{
		retString = ""
		try {
			sKey = argObj1 + "^" + argObj2;
			retString = top.iWallet.sTradcode[sKey];
			if (!retString) retString = "";
			return retString;
		}catch(e) {
			return "";
		}	
	}
}

//OPEN창에서 사용
function getCode2(argObj1,argObj2){
	Key1 = argObj1;
	Key2 = argObj2;
	if(opener.top.iWallet.LoadCode_state == false){
		opener.top.iWallet.LoadCode = opener.top.iWallet.LoadCode + 1;
		if(opener.top.iWallet.LoadCode <= 5){
			setTimeout("getCode2(Key1,Key2)", 1000)
		}else{
			alert("코드 초기화에 실패하였습니다.\n처음으로 돌아갑니다.")
			SID = get_sessionid();
			CryptoClient.Logout(SID);	
			top.location.href="/btindex.asp";
		}
	}else{
		retString = ""
		try {
			sKey = argObj1 + "^" + argObj2;
			retString = opener.top.iWallet.sTradcode[sKey];
			if (!retString) retString = "";
			return retString;
		}catch(e) {
			return "";
		}	
	}
}

function BT_LoadCode(){
	var dgbcode=ttradcode();
	tmp1 = dgbcode.split("^^^");
	for(k=0;k<tmp1.length;k++){
		tmp2 = tmp1[k].split("^^");
		top.iWallet.sTradcode[tmp2[0]]=tmp2[1];
	}
}

//코드구분, 셀렉트이름, selected값
function BT_getComboData(argObj1,selname,argObj2){ 
	Key1 = argObj1;
	Key2 = selname;
	Key3 = argObj2;

	if(top.iWallet.LoadCode_state == false){
		top.iWallet.LoadCode = top.iWallet.LoadCode + 1;
		if(top.iWallet.LoadCode <= 5){
			setTimeout("BT_getComboData(Key1,Key2,Key3)", 1000)
		}else{
			alert("코드 초기화에 실패하였습니다.\n처음으로 돌아갑니다.")
			SID = get_sessionid();
			CryptoClient.Logout(SID);	
			top.location.href="/btindex.asp";
		}
	}else{
		try {
				var data=top.iWallet.ttradcode();
				var split_data=data.split("^^^");
				var len = split_data.length;
				var L_ListText = "<select name=\"" + selname + "\" id=\"" + selname + "\">";
				

				for(i=0;i<len;i++) {
					var splitdata_in=split_data[i].split("^^");
					var splitdata_in2=splitdata_in[0].split("^");

					if(splitdata_in2[0] == argObj1) 
					{
						if (splitdata_in2[1] == argObj2) {
							L_ListText += "\t<option value=\"" + splitdata_in2[1] + "\" selected>" + splitdata_in[1] + "</option>\n";
						} else {
						L_ListText += "\t<option value=\"" + splitdata_in2[1] + "\">" + splitdata_in[1] + "</option>\n";
						}
					}
					
				}

				L_ListText = L_ListText + "</select>"
				return L_ListText;
			}catch(e) {
			return false;
			}		
	}
}

function btAlertNoValue ( thisvalue , alertvalue )
{
	if ( thisvalue.value == "" )
	{
		var smsg;
		smsg = alertvalue + "을(를) 입력하셔야 합니다.";
		alert(smsg);
		thisvalue.focus();
		return 1;
	}

	return 0;
}


function btMov_AddList(argObj, argData, argValue,selval){
	
	argObj.length = argObj.length + 1	
	argObj.options(argObj.length-1).text = argData
	argObj.options(argObj.length-1).value = argValue
	if(selval != ""  && selval == argValue) argObj.options(argObj.length-1).selected = true;

}

//숫자외의 문자 check
function checkNumber(data) {
	t = data.value ;
	for(i=0;i<t.length;i++) 
		if (t.charAt(i)<'0' || t.charAt(i)>'9') {
		if (t.charAt(i) != '*') {
			alert("숫자만 입력해주세요.") ;
			data.value="";
			data.focus() ;
			return false ;
		}
	}
}


function btTextCheckDate(Dnum,StaYer,StaMon,StaDay,EndYer,EndMon,EndDay)
{

    	now = new Date();
    	var cur_year= now.getYear();
		var cur_month =  now.getMonth();
		var month =  now.getMonth()+1;
		month=new String(month);
		var cur_day = now.getDate();
		cur_day=new String(cur_day);
    	var d_cur_date = new Date(cur_year,cur_month,cur_day);
		var dategubun;
    	
		var num = Dnum;
	
       	eval("doc."+StaYer+".readOnly = true");
       	eval("doc."+StaMon+".readOnly = true");
       	eval("doc."+StaDay+".readOnly = true");
       	eval("doc."+EndYer+".readOnly = true");
       	eval("doc."+EndMon+".readOnly = true");
       	eval("doc."+EndDay+".readOnly = true");
		if(num==0){	//당일
			var before_date = new Date(Date.parse(d_cur_date) - 1*1000*60*60*24);
			dategubun = "1주일전";
		}else if(num==1){	//1주일전
			var before_date = new Date(Date.parse(d_cur_date) - 7*1000*60*60*24);
			dategubun = "1주일전";
		}else if(num==2){	//15일전
			var before_date = new Date(Date.parse(d_cur_date) - 15*1000*60*60*24);
			dategubun = "15일전";
		} else if(num==3){	//30일
			var before_date = new Date(Date.parse(d_cur_date) - 30*1000*60*60*24);
			dategubun = "30일";
		} else if(num==4){	//기간설정(3개월)	
			var before_date = new Date(Date.parse(d_cur_date) - 91*1000*60*60*24);
       			eval("doc."+StaYer+".readOnly = false");
       			eval("doc."+StaMon+".readOnly = false");
       			eval("doc."+StaDay+".readOnly = false");
       			eval("doc."+EndYer+".readOnly = false");
       			eval("doc."+EndMon+".readOnly = false");
       			eval("doc."+EndDay+".readOnly = false");		
			dategubun = "기간설정";
		}
	
		var yest_date = new Date(Date.parse(d_cur_date)-1000*60*60*24);
		var yest_year = new String(yest_date.getYear());
		var yest_month = new String(yest_date.getMonth()+1);
		
		
		if(yest_month.length==1){
			yest_month = '0'+yest_month;
		}
    	
		if(month.length==1){
			month = '0'+ month;
		}
    	
    	
		if(cur_day.length==1){
			cur_day = '0'+cur_day;
		}
    	
		var before_year = new String(before_date.getYear());
		var before_month = new String(before_date.getMonth() +1);
		if(before_month.length==1){
			before_month = '0'+before_month;
		}
		var before_day = new String(before_date.getDate());
		if(before_day.length==1){
			before_day = '0'+before_day;
		}

  		if(num==0){
    	   		eval("doc."+StaYer+".value=cur_year");
    	   		eval("doc."+StaMon+".value=month");
    	   		eval("doc."+StaDay+".value=cur_day");
    	   		eval("doc."+EndYer+".value=cur_year");
    	   		eval("doc."+EndMon+".value=month");
    	   		eval("doc."+EndDay+".value=cur_day");
    	   	}
    	   	else if(num==4){
    	 		eval("doc."+StaYer+".value=''");
    	   		eval("doc."+StaMon+".value=''");
    	   		eval("doc."+StaDay+".value=''");
    	   		eval("doc."+EndYer+".value=''");
    	   		eval("doc."+EndMon+".value=''");
    	   		eval("doc."+EndDay+".value=''");              		
		}	
    	   	else
    	{
    		eval("doc."+StaYer+".value=before_year");
    	 		eval("doc."+StaMon+".value=before_month");
    	 		eval("doc."+StaDay+".value=before_day");
    	 		eval("doc."+EndYer+".value=cur_year");
    	 		eval("doc."+EndMon+".value=month");
    	 		eval("doc."+EndDay+".value=cur_day");
    	}
}


//*--  필드 AutoSkip  --*//
function btAutoSkip(){

	var  el = event.srcElement
	if ((el.value == null) || (event.keyCode==13)) return(false)
   
	// 방향키 무시
	var sKeys = "8;16;46;;37;38;39;40;33;34;35;36;45;229;"
	if (sKeys.indexOf(event.keyCode+";") > -1) return;

	if  (el.tagName == "INPUT" )
		if  (el.value.length  >=  el.maxLength ) {
			var i=0
			while (el != el.form.elements[i] )  i++
			if ( (i+1) != el.form.elements.length ) {
				while ( ( el.form.elements[i+1].type == "hidden" ) || 
					( el.form.elements[i+1].type == "button" ) || 
					( el.form.elements[i+1].type == "checkbox" ) || 
					( el.form.elements[i+1].type == "radio" ) || 
					( el.form.elements[i+1].tagName == "FIELDSET" ) || 
					( el.form.elements[i+1].style.display == "none" )  || 
					( el.form.elements[i+1].disabled == true ) ||
					( el.form.elements[i+1].style.visibility == "hidden" )) {
				i++
				if ( (i+1) == el.form.elements.length ) return;
			}
			}
			else{
				el.form.elements[i].focus();
				return(false);
			}
        
			el.form.elements[i+1].focus()
			if(el.form.elements[i+1].tagName != "SELECT") el.form.elements[i+1].select()
			else if(el.form.elements[i+1].tagName == "SELECT") {
				if(el.form.elements[i+1].value==null || el.form.elements[i+1].selectedIndex==-1)
            				el.form.elements[i+1].value = "";
			}
		}
}

//계좌번호에 하이픈 넣기 - pansory
function BT_makeAcntnoDash (val) {
	var DashedAcntno = val	
	if (val.length == 11) { //계좌번호가 11자리일 경우
		DashedAcntno = val.substring(0,3) + "-" + val.substring(3,5) + "-" + val.substring(5)
	} else if (val.length == 12){
		DashedAcntno = val.substring(0,3) + "-" + val.substring(3,5) + "-" + val.substring(5,11) + "-" + val.substring(11)
	}else if(val.length == 13) { //계좌번호가 13자리일 경우
		DashedAcntno = val.substring(0,3) + "-" + val.substring(3,5) + "-" + val.substring(5,11) + "-" + val.substring(11)
	} else if (val.length == 14) { //계좌번호가 14자리일 경우
		DashedAcntno = val.substring(0,3) + "-" + val.substring(3,5) + "-" + val.substring(5,11) + "-" + val.substring(11)
	} else if (val.length == 16){
		DashedAcntno = val.substring(0,3) + "-" + val.substring(3,5) + "-" + val.substring(5,11) + "-" + val.substring(11)
	}	
	return DashedAcntno;
}

//계좌번호 끝3자리에서 000 제거
function BT_CheckLastAcctno(val){
	var TempAccntno = val;
	var len = val.length;
	var tempAcc = len - 3;
	var lastAccNo = val.substring(tempAcc, len);
	
	if (lastAccNo == "000"){		
		TempAccntno = BT_makeAcntnoDash(val.substring(0, tempAcc));
	} else {		
		TempAccntno = BT_makeAcntnoDash(TempAccntno);
	}
	return TempAccntno;
}

//계좌번호 선택..
function BT_SelectAcntno(USR_AccsList, NeedAcntno) {
	
	var no = NeedAcntno.length;
	var Acntno_No = USR_AccsList.length;
	var SelectedAcntno = new Array();
	var k = 0;

	if (no == 0) {
		SelectedAcntno = USR_AccsList;
		//나중에 확인 필요함.. 고른 것들 중에 첫번째 것을 세션에 넣기..????		
		save_session("ACNTNO", USR_AccsList[0]);
		save_flush();
		
	} else {
		for (var i=0; i < Acntno_No ; i++) {
			
			for(var j=0 ; j < no; j++) {
				
					if(USR_AccsList[i].length > 12) {
						stCode = USR_AccsList[i].substring( 9, 11);
						
					} else {
						stCode = "0"; //Case Error
					}
					
					if (stCode == NeedAcntno[j]) {
						SelectedAcntno[k] = USR_AccsList[i];
						
						
						
						if (k == "0") {
							//나중에 확인 필요함.. 고른 것들 중에 첫번째 것을 세션에 넣기..????		
							save_session("ACNTNO", USR_AccsList[i]);
							save_flush();
						}
						
						k++;
					}
			}
		}
	}
	
	return SelectedAcntno;
	
}

//계좌번호 Array를 셀렉트 박스로 변환 - pansory
//조건에 따라(0: 조회, 1: 이체가능, 2: 저축성, 4: 대출, 5: 신탁, 6: 기타)
function BT_MakeAcntList(val, selectedval,selname,condition) {
	var Acct = BT_KindAcct(val,condition);
	var tempAcct = Acct.split(",");
	
	var L_ListText = "<select name=\"" + selname + "\" id=\"" + selname + "\">";

	for(var i=0; i< tempAcct.length - 1; i++)  {
		if (tempAcct[i] == selectedval) {
			
			L_ListText += "\t<option value=\"" + tempAcct[i] + "\" selected>" + BT_makeAcntnoDash(tempAcct[i]) + "</option>\n";
		} else {
			L_ListText += "\t<option value=\"" + tempAcct[i] + "\">" + BT_makeAcntnoDash(tempAcct[i]) + "</option>\n";
		}
		
	} 
	
	L_ListText += "</select>\n";

	return L_ListText;
}


//계좌종류별로계좌번호가져오기
/*[1] 11      -> /s100/bc101_r2.jsp  (1080) (계좌상세조회 결과와 같은 화면 사용)   
[2] 19 ~ 28 -> /s100/bc102_r3.jsp  (1020) (적립식예금)   
[3] 31 ~ 34 -> /s400/bc419_r1.jsp  (4190) (외화예금)   
[4] 40 ~ 49 -> /s100/bc102_r2.jsp  (1130) (대출계좌)   
[5] 50 ~ 73 -> /s100/bc102_r1.jsp  (1020) (기타)   
[6] 그 외   -> /s100/bc1090_r1.jsp (1090) (요구불예금)*/
function BT_KindAcct(Acct,kind)
{
	var tmpAcct = "";//new Array;
	var flag = new Array;

	flag = get_session("IGBGYEJI");

	for(i=0; i< Acct.length; i++){
		if(kind == ""){
			tmpAcct = tmpAcct + Acct[i] + ",";
		}else if(BT_StrToInt(kind) == BT_StrToInt(flag[i])) {
		   tmpAcct = tmpAcct + Acct[i] + ",";
		}
	}
	return tmpAcct;
}

// "-" 삭제 - pansory
function BT_ReduceDash(val) 
{
	var x, ch;
	var i=0;
	var newVal="";
	for(x=0; x <val.length ; x++){
		ch=val.substring(x,x+1);
		if(ch != "-")  newVal += ch;
	}
	return newVal;
} 


//오늘 날짜열 만들기 - pansory
function BT_getToday() {
	var today = new Date();
	var yyyy, mm, dd;
	var Today;
	
	yyyy = today.getYear();
	mm = BT_make2Length(today.getMonth() + 1);
	dd = BT_make2Length(today.getDate());
	
	Today = yyyy.toString() + mm.toString() + dd.toString();
	
	return Today;
}


// 월일을 두자리로 만들기 - pansory
function BT_make2Length(val) {
	if(val < 10) {
		val = "0" + val;
	}
	return val;
}

//공백 없애기
function BT_TrimSpace(str) { 
	var count = str.length; 
	var len = count;                 
	var st = 0; 

	while ((st < len) && (str.charAt(st) <= ' ')) { 
		st++; 
	} 
	while ((st < len) && (str.charAt(len - 1) <= ' ')) { 
		len--; 
	}                 
	return ((st > 0) || (len < count)) ? str.substring(st, len) : str ;   
}

//공백 없애기
function BT_TrimSpace1(str) { 
	var count = str.length; 
	var len = count;                 
	var st = 0; 
	
	while ((st < len) && (toString(str.charAt(st)) <= toString(' '))) { 
		st++; 
	} 
	while ((st < len) && (toString(str.charAt(len - 1)) <= toString(' '))) { 
		len--; 
	}        
	
	return ((st > 0) || (len < count)) ? str.substring(st, len) : str ;   
	
}

//session의 PAY_ACNTNO 와 ACNTNO(val) 값 비교
function BT_ComparePayAcntno(val) {
	
	var USR_AccsList = get_session("ACCS");
	var check = "";
	
	for (var i=0; i < USR_AccsList.length; i++) {
		if (USR_AccsList[i] == val) {
			check =  "1";
		}
	}
	return check;
}

// 금액 더하기 계산하기
function BT_calc(string,name) 
{ 
	var sum = 0;
	BT_Del_comma(doc.form.REC_AMT);

	if (eval(string) == 0 ) {
		doc.form.REC_AMT.value = "";
	}
	else {
		sum = doc.form.REC_AMT.value;
		if (sum == "") {
			sum = 0;
		}
		sum = eval(sum) + eval(string);
		doc.form.REC_AMT.value = sum;
	}	

	doc.form.REC_AMT.focus();
}

// 금액 더하기 계산하기
function BT_Card_calc(string,name) 
{ 
	var sum = 0;
	BT_Del_comma(doc.form.SEV_AMT);

	if (eval(string) == 0 ) {
		doc.form.SEV_AMT.value = "";
	}
	else {
		sum = doc.form.SEV_AMT.value;
		if (sum == "") {
			sum = 0;
		}
		sum = eval(sum) + eval(string);
		doc.form.SEV_AMT.value = sum;
	}	

	doc.form.SEV_AMT.focus();
}



//이체 비밀번호 체크
function BT_isTrsfPass(val)
{
// 이체비밀번호는 4자리이어야 한다.(자리수 검사는 따로 해야 한다.)
// 영문자가 1개이상 포함되어야 하며, 영문자와 숫자의 조합만 허락한다.
	var len = val.length;
	var i;
	var alphanum = 0;
	
	for(i=0;i<len;i++) {
		if ( BT_isAlphabet(val.charAt(i)) ) 
			alphanum = alphanum + 1;
		else if ( isNaN(val.charAt(i)) ) 
			return false;
	}
	if ( alphanum > 0 ) 
		return true;
	else
		return false;
}

//알파벳인지 체크 - 이체비밀번호 체크와 세트
function BT_isAlphabet(val)
{
	var alphaStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	if ( alphaStr.indexOf(val) < 0 ) 
		return false;
	else 
		return true;
}

//숫자만 있는지 체크 
function BT_isNumber(val)
{
	var numberStr = "0123456789";
	
	if ( numberStr.indexOf(val) < 0 ) 
		return false;
	else 
		return true;
}

function BT_number(val){

		if ((event.keyCode<48)||(event.keyCode>57 )){		
			if(event.keyCode != 8  ) // 백스페이스 제외
			{
				event.returnValue=false;
			}
		}
}


// 금액 입력시 "," 자동 입력 & 우측 정렬
function BT_Add_comma(REC_AMT)
{				

	if(!BT_isAllNumber(REC_AMT.value))
	{
		alert("금액을 숫자로 입력해 주세요.");
		REC_AMT.value = "";
		REC_AMT.focus();
		return false;
	}
	var src 
    	var i;
	var factor;
	var su;
	var Spacesize = 0;

	factor = REC_AMT.value.length % 3;
	su = (REC_AMT.value.length - factor) /3;
    src = REC_AMT.value.substring(0,factor);

   for(i=0; i<su ; i++)
	{
	   if ((factor==0)&&(i==0))// " XXX "인 경우
		{
			 src += REC_AMT.value.substring(factor+(3*i), factor+3+(3*i));
		}
		else 
		{
			src +=",";
			src += REC_AMT.value.substring(factor+(3*i), factor+3+(3*i));
        }
    }
  REC_AMT.value=src;
  return true;
}

function BT_Add_NumberComma(val) {
	var Ret_val = BT_StrToInt(val);
	Ret_val = BT_Add_Displaycomma(Ret_val);
	
	return Ret_val;
}

function BT_Add_NumberComma1(val) {
	var Ret_val = BT_StrToInt(val);
	var Zerodel = Ret_val;
	if (Zerodel == "0")
		Ret_val = "";
		
	else
		Ret_val = BT_Add_Displaycomma(Ret_val);
	
	return Ret_val;
}

function BT_Add_Number(val) {
	var Ret_val = BT_StrToInt(val);	
	return Ret_val;
}
function BT_Add_Displaycomma(val) {

	var src;
    var i;
	var factor;
	var su;
	var Spacesize = 0;
	
	var String_val = val.toString();
	
	factor = String_val.length % 3;
	su = (String_val.length - factor) /3;
    src = String_val.substring(0,factor);

   for(i=0; i<su ; i++)
	{
	   if ((factor==0)&&(i==0))// " XXX "인 경우
		{
			 src += String_val.substring(factor+(3*i), factor+3+(3*i));
		}
		else 
		{
			if ( String_val.substring(factor+(3*i) - 1, factor+(3*i)) != "-" ) src +=",";
			src += String_val.substring(factor+(3*i), factor+3+(3*i));
        }
    }
  return src;
}

function BT_Add_Card_comma(SEV_AMT)
{

	if(!BT_isAllNumber(form.SEV_AMT.value))
	{
		alert("금액을 숫자로 입력해 주세요.");
		form.SEV_AMT.value = "";
		form.SEV_AMT.focus();
		return false;
	}
	var src 
    	var i;
	var factor;
	var su;
	var Spacesize = 0;

	factor = SEV_AMT.value.length % 3;
	su = (SEV_AMT.value.length - factor) /3;
    src = SEV_AMT.value.substring(0,factor);

   for(i=0; i<su ; i++)
	{
	   if ((factor==0)&&(i==0))// " XXX "인 경우
		{
			 src += SEV_AMT.value.substring(factor+(3*i), factor+3+(3*i));
		}
		else 
		{
			src +=",";
			src += SEV_AMT.value.substring(factor+(3*i), factor+3+(3*i));
        }
    }
  SEV_AMT.value=src;
  return true;
}

//전체 값이 숫자로만 되어 있는지 체크
function BT_isAllNumber(val)
{
	var len = val.length;
	var i=0;
	
	for(i=0;i<len;i++) {
		if ( isNaN(val.charAt(i)) ) return false;
	}
	return true;
}

// 컴마(",") 자동 삭제
function BT_Del_comma(REC_AMT) 
{
	REC_AMT.value = BT_Reduce_comma(REC_AMT.value);
	return true;
}

// 컴마(",") 자동 삭제
function BT_Del_Card_comma(SEV_AMT) 
{
	SEV_AMT.value = BT_Reduce_comma(SEV_AMT.value);
	return true;
}

function BT_Reduce_comma(account_number) 
{
	var x, ch;
	var i=0;
	var newVal="";
	for(x=0; x <account_number.length ; x++){
		ch=account_number.substring(x,x+1);
		if(ch != ",")  newVal += ch;
	}
	return newVal;
} 

function BTCom_Replace(originalString, findText, replaceText){

	var pos = 0
	var preStr = ""
	var postStr = ""

	pos = originalString.indexOf(findText)
	
	while (pos != -1) {
		preString = originalString.substr(0,pos)
		postString = originalString.substring(pos+findText.length)
		originalString = preString + replaceText + postString
		pos = originalString.indexOf(findText)
	}
	
	return originalString
}

function BT_getRandomNumber() { //씨크릿 카드 순차번호 생성
	k=Math.round(Math.random()*30)
	if(k == 0) k = k + 1
	var L_Number = BT_make2Length(k);
	return L_Number;
}


//9자리 고객번호에 하이픈 넣기 - pansory
function BT_makeCustnoDash (val) {
	DashedCustno = val.substring(0,3) + "-" + val.substring(3,9);
	return DashedCustno;
}

//주민번호에 하이픈 넣기
function BT_makeResnoDash (val) {
	if (val.length == 10) { //기업의 경우
		DashedResno = val.substring(0,3) + "-" + val.substring(3,5) + "-" + val.substring(5);
	} else { //개인의 경우
		if (val.substring(0,3) == "000"){
			DashedResno = val.substring(3,6) + "-" + val.substring(6,8) + "-" + val.substring(8,13);
		} else {
			DashedResno = val.substring(0,6) + "-" + val.substring(6,13);
		}
		
	}
	return DashedResno;
}


//카드번호에 하이픈 넣기 - pansory
function BT_makeCardnoDash (val) {
		DashedAcntno = val.substring(0,4) + "-" + val.substring(4,8) + "-" + val.substring(8,12) + "-" + val.substring(12,16);
	return DashedAcntno;
}

//회원번호(16자리)에 하이픈 넣기 - pansory
function BT_makeMembernoDash (val) {
		DashedAcntno = val.substring(0,4) + "-" + val.substring(4,8) + "-" + val.substring(8,12) + "-" + val.substring(12,16);
	return DashedAcntno;
}

//카드번호 Array를 셀렉트 박스로 변환 - pansory
function BT_MakeCardList(val, selectedval) {
	
	var L_ListText = "<select name=\"ACNTNO\" id=\"ACNTNO\">";

	for(var i=0; i< val.length; i++)  {
		if (val[i] == selectedval) {
			L_ListText += "\t<option value=\"" + val[i] + "\" selected>" + BT_makeCardnoDash(val[i]) + "</option>\n";
		} else {
			L_ListText += "\t<option value=\"" + val[i] + "\">" + BT_makeCardnoDash(val[i]) + "</option>\n";
		}
		
	} 
	
	L_ListText += "</select>\n";

	return L_ListText;
}

//카드번호 Array를 셀렉트 박스로 변환 - pansory
function BT_makeCardnoList(val) {
	
	var L_ListText = "<select name=\"CARD_NO\" id=\"CARD_NO\">";

	for(var i=0; i< val.length; i++)  {
		if (i == 0) {
			L_ListText += "\t<option value=\"" + val[i] + "\" selected>" + BT_makeCardnoDash(val[i]) + "</option>\n";
		} else {
			L_ListText += "\t<option value=\"" + val[i] + "\">" + BT_makeCardnoDash(val[i]) + "</option>\n";
		}
		
	} 
	
	L_ListText += "</select>\n";

	return L_ListText;
}

//카드번호 선택..
function BT_SelectCardno(USR_AccsList, NeedCardno) {
	
	var no = NeedCardno.length;
	var Cardno_No = USR_AccsList.length;
	var SelectedCardno = new Array();
	var k = 0;

	if (no == 0) {
		SelectedCardno = USR_AccsList;
		//나중에 확인 필요함.. 고른 것들 중에 첫번째 것을 세션에 넣기..????		
		save_session("CARD_NO", USR_AccsList[0]);
		save_flush();
		
	} else {
		for (var i=0; i < Cardno_No ; i++) {
			
			for(var j=0 ; j < no; j++) {
				
					if(USR_AccsList[i].length > 12) {
						stCode = USR_AccsList[i].substring( 9, 11);
						
					} else {
						stCode = "0"; //Case Error
					}
					
					if (stCode == NeedAcntno[j]) {
						SelectedCardno[k] = USR_AccsList[i];
						
						if (k == "0") {
							//나중에 확인 필요함.. 고른 것들 중에 첫번째 것을 세션에 넣기..????		
							save_session("CARD_NO", USR_AccsList[i]);
							save_flush();
						}
						k++;
					}
			}
		}
	}
	return SelectedCardno;
}

function BT_makeLoannoDash (val) {
		DashedLoanno = val.substring(0,3) + "-" + val.substring(3,9) + "-" + val.substring(9,11) + "-" + val.substring(11,16)
	return DashedLoanno;
}

function BT_makeDateDash (val) {
		if (val == "00000000") 
			DashedDate = "";
		else 
			DashedDate = val.substring(0,4) + "-" + val.substring(4,6) + "-" + val.substring(6,8);
			
	return DashedDate;
}

function BT_makeDateDash_1 (val) {
		if (val == "000000") 
			DashedDate_1 = "";
		else 
			DashedDate_1 = val.substring(0,4) + "-" + val.substring(4,6);
			
	return DashedDate_1;
}
function BT_makeDateDash_2 (val) {
		if (val == "00000") 
			DashedDate_2 = "";
		else 
			DashedDate_2 = val.substring(0,2) + "-" + val.substring(2);
			
	return DashedDate_2;
}

function BT_makeDateDash_3 (val) {
		if (val == "00000000") 
			DashedDate_3 = "";
		else 
			DashedDate_3 = val.substring(6,8);
			
	return DashedDate_3;
}

function BT_makeDateSlash (val) {
		if (val == "00000000") 
			DashedDate_1 = "";
		else 
			DashedDate_1 = val.substring(0,4) + "/" + val.substring(4,6);
			
	return DashedDate_1;
}

function BT_makeTime (val) {
		ReturnTime = val.substring(0,2) + ":" + val.substring(2,4) + ":" + val.substring(4,6);
	return ReturnTime;
}

//문자열을 10진수로 변환
function BT_StrToInt(val) {
	var Re_int = 0
	
	if (val != "")
		var Re_int = parseInt(val,10);
	else
		Re_int = 0;
	
	return Re_int;
}



function BT_makeDecimalPoint(val) {
	var ReturnVal = val.split(".");
	ReturnVal = BT_Add_NumberComma(ReturnVal[0]) + "." + ReturnVal[1];
	return ReturnVal;
	
}


//페이지 설정
function last_page(val){
	var last_page = val%10;
	var index = parseInt(val/10);

	if (last_page > 0){
		return(index+1);
	}else
		return(index);
}	


//소수점이하 자리수 설정
function BT_makeCipherPoint(val,cipher)
{
	
	var PreCipher = val.length - cipher;

	var Preval = val.substring(0,PreCipher);
	var Nextval = val.substring(PreCipher);
	var Returnval = Preval + "." + Nextval;

	return Returnval
}

//숫자와 영어만 허용
function BT_EngNum(obj,isUP){

	//if (event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) return;//에러 발생 수정함.
	var returnValue = "";
	for (var i = 0; i < obj.value.length; i++){
		if ((obj.value.charAt(i) >= "0" && obj.value.charAt(i) <= "9") || (obj.value.charAt(i) >= "a" &&  obj.value.charAt(i) <= "z") || (obj.value.charAt(i) >= "A" && obj.value.charAt(i) <= "Z") ){
			returnValue += obj.value.charAt(i);
		}else{
			returnValue += "";
		}
	}

	obj.value = returnValue;
	obj.focus();
}


//날짜 계산 -- jjung
function BT_getDate(Dnum, gubun, StaYer,StaMon,StaDay,EndYer,EndMon,EndDay){
	var cur_year, cur_month, cur_day, cur_hour, cur_min, cur_sec, d_cur_date
	var index=0;
	var Mnum=0;

	var theDate = btGetCurrTime(2,1);
	var now = new Date();
	var daynum = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
	var monname = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');

	if(theDate == ""){
   		cur_year= now.getYear();
		cur_month =  now.getMonth() + 1;
		cur_month = new String(cur_month);
		cur_day = new String(now.getDate());
		cur_hour = theDate.substring(8,10);
		cur_min = theDate.substring(10,12);
		cur_sec = theDate.substring(12);
	}	
    else {
		cur_year = theDate.substring(0,4);
		cur_month = theDate.substring(4,6);
		cur_day = theDate.substring(6,8);
		cur_hour = theDate.substring(8,10);
		cur_min = theDate.substring(10,12);
		cur_sec = theDate.substring(12,14);
	}
    
	daynum[1] = cday(cur_year);
    change_month = monname[BT_StrToInt(cur_month) - 1];
	tmpDate = new String(change_month + cur_day + "," + cur_year + " " + cur_hour + ":" + cur_min + ":" + cur_sec );
	d_cur_date = new Date(tmpDate);

	if(gubun == "mon"){
		tmpMonth = BT_StrToInt(cur_month) + Dnum;
		if (tmpMonth == 0)
		{
			tmpMonth = "12";
			change_month = monname[BT_StrToInt(tmpMonth) - 1];
			tmpDate = new String(change_month + cur_day + "," + (cur_year - 1) + " " + cur_hour + ":" + cur_min + ":" + cur_sec );
		}
		else if (tmpMonth == -1)
		{
			tmpMonth = "11";
			change_month = monname[BT_StrToInt(tmpMonth) - 1];
			tmpDate = new String(change_month + cur_day + "," + (cur_year - 1) + " " + cur_hour + ":" + cur_min + ":" + cur_sec );
		}
		else {
			change_month = monname[BT_StrToInt(tmpMonth) - 1];
			tmpDate = new String(change_month + cur_day + "," + cur_year + " " + cur_hour + ":" + cur_min + ":" + cur_sec );
		}
		before_date = new Date(tmpDate);
	}else{
		var before_date = new Date(Date.parse(d_cur_date) + BT_StrToInt(Dnum)*1000*60*60*24);
	}

	if(theDate.length==0) {			
		var before_year = new String(before_date.getYear());
		var before_month = new String(before_date.getMonth() + 1);

	}else {
		var before_year = new String(before_date.getYear());
		var before_month = new String(before_date.getMonth() + 1);
	}	

	if(before_month.length==1){
		before_month = '0'+before_month;
	}
	var before_day = new String(before_date.getDate());

	if(before_day.length==1){
		before_day = '0'+before_day;
	}
	if(StaYer != "") eval("doc."+StaYer+".value=before_year");
	if(StaMon != "") eval("doc."+StaMon+".value=before_month");
	if(StaDay != "") eval("doc."+StaDay+".value=before_day");
	
	if(EndYer != "") eval("doc."+EndYer+".value=before_year");
	if(EndMon != "") eval("doc."+EndMon+".value=before_month");
	if(EndDay != "") eval("doc."+EndDay+".value=before_day");

	if(StaYer == "" && StaMon == "" && StaDay == "" && EndYer == "" && EndMon == "" &&  EndDay == "") return before_year + before_month + before_day;

}

//val : 전체 값, point : 소수점 앞에 자리수
function BT_toMoneyF(val,point,gubun){
	var foreVal = 0;
	var downVale = 0;
	if (BT_TrimSpace(val) == ""){
		return foreVal;
	} else {
		if(point == "" || point == "0") {
			foreVal = val;
			downVal = "";
		} else {
			if(gubun == "1"){
				foreVal = val.substring(0,point);
			} else{
				foreVal = BT_Add_NumberComma(val.substring(0,point));
			}
			downVal = val.substring(point);
		}
		return foreVal + "." + downVal;
	}
}


function BT_SetServiceCode(code){
	save_session("SERVICE_CODE",code);				
	save_flush();					
}

function BT_Setreturn_url(val){
	save_session("RETURN_URL",val);				
	save_flush();					
}

//2년 윤년처리
function cday(year)
{
	//2월달일때 윤년 처리
	if ((year % 4) == 0) { //윤년
		if ((year % 100) == 0) { //평년
			if ((year % 400) == 0) { //윤년
				daynum = 29;
			}
			else {//평년
				daynum = 28;
			}
		}
		else { //윤년
			daynum = 29;
		}
	}
	else {//평년
		daynum = 28;
	}
	return daynum;
} 

/**
* 입력한 숫자 및 영문(조합도 상관없음)이 비밀번호로 사용 가능한지 다음과 같은 제한사항을
* 검사하는 메소드
* 1. 숫자의 경우 하나의 숫자 조합
* 2. 숫자의 경우 오름차순 및 내림차순
* 3. 영문의 경우 하나의 영문 조합(대소문자는 다른 문자로 인식함)
* 4. 영문의 경우 오름차순 및 내림차순
* @obj 입력폼의 object
* @return true : 비밀번호로 사용가능
* @return false : 비밀번호로 사용할 수 없음. 각 상황에 맞는 메시지가 내장되어 있음.
*/
function pwCheck(obj)
{
	var val = obj.value;
	var flag = false;

	for(i=0;i<val.length;i++)
	{
		if(!BT_isNumber(val.charAt(i)))//숫자 체크
		{
			flag = true;
			break;
		}
	}

	if(flag)//숫자와 영문 혼합 or 영문 조합으로 예상됨
	{
		for(i=0;i<val.length;i++)
		{
			if(!BT_isAlphabet(val.charAt(i)))//영문 체크
			{
				flag = false;
				break;
			}
		}

		if(flag)//*** 영문으로만 조합 연번 체크 요구됨 ***
		{
			if(check_alpha_all_dup(val))//*** 같은 영문자만의 조합 검사 ***
			{
				alert("입력하신 비밀번호가 모두 같은 영문입니다.");
				return false;
			}
			else//*** 오름 차순 및 내림 차순 검사 ***
			{
				if(check_alpha_asc(val))//*** 오름차순 검사 ***
				{
					alert("입력하신 비밀번호가 영문 오름차순입니다.");
					return false;
				}
				else//*** 내림차순 검사 ***
				{
					if(check_alpha_desc(val))
					{
						alert("입력하신 비밀번호가 영문 내림차순입니다.");
						return false;
					}
					else//*** 비밀번호로 사용가능 ***
					{
						return true;
					}
				}
			}
		}
		else//*** 영문 및 숫자 조합 - 비밀번호로 상용가능 ***
		{
			return true;
		}
	}
	else//*** 숫자로만 조합되었으므로 연번 체크 요구됨 ****
	{
		if(check_digit_all_dup(val))//*** 같은 숫자만의 조합 검사 ***
		{
			alert("입력하신 비밀번호가 모두 같은 숫자입니다.");
			return false;
		}
		else//*** 오름 차순 및 내림 차순 검사 ***
		{
			if(check_digit_asc(val))//*** 오름차순 검사 ***
			{
				alert("입력하신 비밀번호가 오름차순입니다.");
				return false;
			}
			else//*** 내림차순 검사 ***
			{
				if(check_digit_desc(val))
				{
					alert("입력하신 비밀번호가 내림차순입니다.");
					return false;
				}
				else//*** 비밀번호로 사용가능 ***
				{
					return true;
				}
			}
		}
	}
}


/**
* 숫자로만 구성된 문자열이 내림차순의 숫자들인지 확인
* @return true : 내림차순이다.
* @return false : 내림차순이 아니다.
*/
function check_digit_desc(val)
{
	var init_cnt = 0;
	var flag = true;

	for(i=0;i<numberArray.length;i++)
	{
		if(val.charAt(0) == numberArray[i])
		{
			init_cnt = i;
			break;
		}
	}

	var init_cnt_tmp = init_cnt;
	for(i=0;i<val.length;i++)
	{
		if(val.charAt(i) == numberArray[init_cnt_tmp])
		{
		}
		else
		{
			flag = false;
			break;
		}
		
		init_cnt_tmp -= 1;
		if(init_cnt_tmp == -1)
		{
			init_cnt_tmp = numberArray.length - 1;
		}
	}

	return flag;
}

/**
* 영문으로만 구성된 문자열이 내림차순인지 확인
* @return true : 내림차순이다.
* @return false : 내림차순이 아니다.
*/
function check_alpha_desc(val)
{
	var init_cnt = 0;
	var flag = true;

	for(i=0;i<alphaArray.length;i++)
	{
		if(val.charAt(0) == alphaArray[i])
		{
			init_cnt = i;
			break;
		}
	}

	var init_cnt_tmp = init_cnt;
	for(i=0;i<val.length;i++)
	{
		if(val.charAt(i) == alphaArray[init_cnt_tmp])
		{
		}
		else
		{
			flag = false;
			break;
		}
		
		init_cnt_tmp -= 1;
		if(init_cnt_tmp == -1)
		{
			init_cnt_tmp = alphaArray.length - 1;
		}
	}

	return flag;
}


/**
* 숫자로만 구성된 문자열이 오름차순의 숫자들인지 확인
* @return true : 오름차순이다.
* @return false : 오름차순이 아니다.
*/
function check_digit_asc(val)
{
	var init_cnt = 0;
	var flag = true;

	for(i=0;i<numberArray.length;i++)
	{
		if(val.charAt(0) == numberArray[i])
		{
			init_cnt = i;
			break;
		}
	}

	var init_cnt_tmp = init_cnt;
	for(i=0;i<val.length;i++)
	{
		if(val.charAt(i) == numberArray[init_cnt_tmp])
		{
		}
		else
		{
			flag = false;
			break;
		}
		
		init_cnt_tmp += 1;
		if(init_cnt_tmp == numberArray.length)
		{
			init_cnt_tmp = 0;
		}
	}

	return flag;
}

/**
* 영문으로만 구성된 문자열이 오름차순 인지 확인
* @return true : 오름차순이다.
* @return false : 오름차순이 아니다.
*/
function check_alpha_asc(val)
{
	var init_cnt = 0;
	var flag = true;

	for(i=0;i<alphaArray.length;i++)
	{
		if(val.charAt(0) == alphaArray[i])
		{
			init_cnt = i;
			break;
		}
	}

	var init_cnt_tmp = init_cnt;
	for(i=0;i<val.length;i++)
	{
		if(val.charAt(i) == alphaArray[init_cnt_tmp])
		{
		}
		else
		{
			flag = false;
			break;
		}
		
		init_cnt_tmp += 1;
		if(init_cnt_tmp == alphaArray.length)
		{
			init_cnt_tmp = 0;
		}
	}

	return flag;
}

/**
* 숫자로만 구성된 문자열이 같은 숫자로 조합되었는지 확인
* @return true : 동일한 값으로 조합. 비밀번호로 사용하면 안됨.
* @return false : 다른 숫자가 적어도 하나 존재. 비밀번호로 사용해도 괜찮음.
*/
function check_digit_all_dup(val)
{
	var flag = true;

	for(i=0;i<numberArray.length;i++)
	{
		flag = true;

		for(j=0;j<val.length;j++)
		{
			if(val.charAt(j) == numberArray[i])
			{
			}
			else
			{
				flag = false;
				break;
			}
		}

		if(flag)
		{
			break;
		}
		else
		{
			continue;
		}
	}

	return flag;
}

/**
* 영문으로만 구성된 문자열이 같은 영문으로 조합되었는지 확인
* @return true : 동일한 영문으로 조합. 비밀번호로 사용하면 안됨.
* @return false : 다른 영문이 적어도 하나 존재. 비밀번호로 사용해도 괜찮음.
*/
function check_alpha_all_dup(val)
{
	var flag = true;

	for(i=0;i<alphaArray.length;i++)
	{
		flag = true;

		for(j=0;j<val.length;j++)
		{
			if(val.charAt(j) == alphaArray[i])
			{
			}
			else
			{
				flag = false;
				break;
			}
		}

		if(flag)
		{
			break;
		}
		else
		{
			continue;
		}
	}

	return flag;
}