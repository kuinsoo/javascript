
### * Set Timeout / ClearTimeout
> 정해진 시간에 한번 실행
### * Set Interval / Clear Interval   
> 반복적인 실행 ( 1 < time = 크롬 1초 강제 )
### * Request Animation Frame 
> 아주빠른 반복을 실행행 할때 ( 브라우저가 업데이트 할때마다 요청 ) \
CPU, 그래픽카드 에 최적화 / 다른 탭으로 이동시 실행 중단

### * Non Blocking
> javascript Non Blocking 언어이며 alert() 는 blocking 함수 이다.
### * Parallel
> 기본적으로 함수는 Stack 에 바로 쌓지만 setTimeout() 등과 같이 javascript 내에서 정해 놓은 몇가지 함수들은 Web Api 로 옮겨지게 되고 queue 로 이동해 다시 stack 으로 쌓이게 된다.

### * IIFE (Immediately-Invoked Function Expressions)
다른 곳에서 코드를 컨트롤 할수 없도록 해준다.
```js
(function() {
    const secretUsers = ['Nicolas', 'Lynn', 'Stevey']
    console.log(secretUsers)
})()
```
### * Modules
> es6 이상에서 사용 가능 하며 javascript 자체 적으로 모듈 기능을 사용 할수 있다.\
type='module' 을 명시 해줘야한다.
```js
// App.js
export let users = ['name', 'phone', 'content']
export const addUser = (user) => users = [...users, user]
export const getUsers = () => users
export const deleteUser = (user) => users = users.fillter(aUser => aUser !== user)

// index.js
import { users, addUser, getUsers, deleteUser } from './app.js';

// main.html
<script type='module' src='App.js'></script>
<script type='module' src='index.js'></script>
```

### * Expression
> expression 은 value 를 가져온다.  함수의 경우 return 을 받거나 없으면 undefined 를 value 로 가져온다
```js
const val = (a, b) => a + b 
```

### * Statement
> 이미 정의 되어있는 if, for, foreach .. 등
```js
if(a===b){

}

for (let index = 0; index < array.length; index++) {
    const element = array[index];
    
}
```
### * declaration
> javascript 는 declaration 을 상단으로 먼저 옮겨 실행 한다.
```js
// declaration
const declaration = add (1, 5)

function add(a, b)
{
    return a + b
}

console.log(declaration)
// 6
```
```js
// Expression  방식 => 에러
const declaration = add (1, 5)

const add = (a, b) => a + b

console.log(declaration)
// ReferenceError: add is not defined as evalmachine.
```

### * coercion
```js
// == 는 양쪽 값을 숫자로 변환 하여 비교
// === coercion 이 이러나지 않음 
```

### * typeof
> 타입을 찾아준다. \
하지만 몇가지 버그가 존재 한다.  그래서 Array 와 Object 는 instanceof 를 사용한다.
```js
console.log(typeof valiable)
console.log(typeof (valiable))

// WTF 버그
console.log(typeof null)  // object
console.log(typeof []) // object

// instanceof 사용 ( Array, Object 작동)
console.log([] instanceof Array) // true 
```
