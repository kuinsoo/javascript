var kim={name:'kim',first:10,second:20}
var lee={name:'lee',first:10,second:10}
function sum(prefix){return prefix+(this.first+this.second);}

// this 고정 시킨다.  sum 에 영향을 주지 않고 새로 생성
var kimSum=sum.bind(kim,'->');  // new kim.sum(this, parameter1 .. );
console.log('kimSum()',kimSum());

