/* Sort 정렬 기능 */

const months = ['Mar', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
months.sort()
console.log(months)

const array1 = [1, 30, 4, 21, 1000]
array1.sort()
console.log(array1) //[ 1, 1000, 21, 30, 4 ]

array1.sort(function(a, b) { 
    if(a < b) return -1 // 음수 오름차순
    else if(a > b) return 1 // 양수 내림차수
    return 0 // 원래 값
})
console.log(array1) // [ 1, 4, 21, 30, 1000 ] 

array1.sort((a, b) => b - a)
console.log(array1) // [ 1000, 30, 21, 4, 1 ] 

// 리스트 sort 정렬 
const userList = [
    {id:1, fullName:'kkuinsoo', phone:'01099470728'},
    {id:2, fullName:'miso', phone:'01099470202'},
]

const newUserList = userList.sort((a, b) => (a.id > b.id ? -1 : 1))
console.log(newUserList);
/* 
[ { id: 2, fullName: 'miso', phone: '01099470202' }, 
  { id: 1, fullName: 'kkuinsoo', phone: '01099470728' } ] 
*/
