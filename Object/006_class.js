class Person {
  constructor(name, first, second) { // 객체 실행시 시작
    this.name = name;
    this.first = first;
    this.second = second;
    console.log('constroctor');
  }
  sum() {
    return 'prototype : ' + (this.first+this.second);
  }
}

var kim = new Person('kim', 10,20);
console.log("kim", kim);
console.log("kim.sum()", kim.sum());
kim.sum = function() {
  return 'this : ' + (this.first-this.second);
}
console.log("kim.sum()", kim.sum());




// // 4. constructor 함수 사용방법
// var kim = new Person('kim',10,20,30);
// kim.sum = function() { // 따로 정의 하여 사용하기 위해서 재설정 
//   return 'this : ' + (this.first+this.second);
// }
// var lee = new Person('lee',10,10,10); // prototype 을 찾아서 실행
// console.log("lee.sum()", lee.sum());