// 1. 객체를 만들어 출력 
var kim = {
  name:'kim',
  first:10,
  second:20,
  sum1:function(f, s){
    return f+s;
  },
  sum2:function(){
    return kim.first+kim.second;
  },
  sum3:function(){
    return this.first+this.second;
  },
}

console.log("kim.sum(kimfirst, kim.second)", kim.sum1(kim.first, kim.second));
console.log("kim.sum() Name : ", kim.sum2());
console.log("kim.sum() This : ", kim.sum3());

var lee = {
  name:'lee',
  first:10,
  second:10,
  sum1:function(f, s){
    return f+s;
  },
  sum2:function(){
    return kim.first+kim.second;
  },
  sum3:function(){
    return this.first+this.second;
  },
}

console.log("lee.sum(lee.first, lee.second)", lee.sum1(lee.first, lee.second));
console.log("lee.sum() Name : ", lee.sum2());
console.log("lee.sum() This : ", lee.sum3());


// 2. Date 객체로 테스트 
var d1 = new Date('2019-04-10');
console.log('d1.getFullYear()', d1.getFullYear()); // Date 객체 년도
console.log('di.getMonth()', d1.getMonth()); // Date 객체 월 ( 숫자로 리턴 )
console.log('Date', Date);

// 3. constructor 함수 생성
function Person(name, first, second, third) {
  this.name=name;
  this.first=first;
  this.second=second;
  this.third=third;
  this.sum = function() {
    return this.first+this.second+this.third;
  }
}

console.log('Person()', Person());
console.log('new Person()', new Person('person', 10,10,10)); // 생성자 호출

// 4. constructor 함수 사용방법
var kim = new Person('kim',10,20,30);
var lee = new Person('lee',10,10,10);
console.log("kim.sum()", kim.sum());
console.log("lee.sum()", lee.sum());


