var kim={name:'kim',first:10,second:20}
var lee={name:'lee',first:10,second:10}
function sum(prefix){return prefix+(this.first+this.second);}

// call  함수안에 this 를 정해준다.
sum.call(kim);
console.log('sum.call(\'kim\') : '+sum.call(kim));
console.log('sum.call(\'lee\') : '+sum.call(lee));

// 인자 값   call( this , parameter1 ... )
console.log('sum.call(\'lee\') : '+sum.call(lee, ' => '));

