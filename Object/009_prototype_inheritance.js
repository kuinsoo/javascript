/* 객체 지향 프로그래밍 */

// class
/* 
   상속 구조 ( 겉으로는 다른 언어와 비슷하지만 내부적으로는 자바스크립트 방식으로 처리 )
   super class <- subclass -> object

   객체(인스턴스) 상속 ( 자유도는 높지만 복잡 )
   super object  <-(prototype link)<-   sub object   
   
   *위와 동일 
   prototype object  <-(prototype link)<-   sub object   sub object 가 가르켜는 링크를 prototype object 라고 도 부른다.
*/

// objec ( instance )

// --------------------------- 실습 -----------------------------------

// prototype 자바스크립트
console.log("===== prototype link 자바스크립트=====");
var superObj={superVal:'super',subVal:'super_sub'}
var subObj={subVal:'sub'} 
subObj.__proto__=superObj;
console.log('subObj.subVal => '+subObj.subVal); // 자신이 가지고 있을 경우 자신 출력
console.log('subObj.superVal => '+subObj.superVal); // 자신이 없을 경우 prototype link 검색 후 있으면 출력
console.log('subObj.superVal => '+subObj.superVang); // 어디에도 없을 경우 undefined 

subObj.superVal="sub_ch_super";
subObj.superVang="sub_superVang";
console.log('super.superVal => '+superObj.superVal); // prototype link 값을 변경시 !! 변화 없음
console.log('sub.superVal => '+subObj.superVal); // *prototype link 값을 변경시(아래결과로 prototype을 가르켜는게 아닌것으로 판단)  !! 자신에 객체가 값은 변하지만 super object 값은 변경되지 않는다.
console.log('subObj.superVang => '+subObj.superVang); // prototype link 값을 변경하는 것이 아니라 자신의 객채를 생성하여 값을 대입한다.


/* 자바스크립트 권장 사용 방법 변경*/
console.log('');
console.log("=====자바스크립트 권장 사용 방법 변경=====");
// Object.create() 사용
// prototype 자바스크립트
var superObj={superVal:'super',subVal:'super_sub'}
// var subObj={subVal:'sub'} 
// subObj.__proto__=superObj;
var subObj = Object.create(superObj);
subObj.subVal='sub';
debugger;
console.log('subObj.subVal => '+subObj.subVal); // 자신이 가지고 있을 경우 자신 출력
console.log('subObj.superVal => '+subObj.superVal); // 자신이 없을 경우 prototype link 검색 후 있으면 출력
console.log('subObj.superVal => '+subObj.superVang); // 어디에도 없을 경우 undefined 

subObj.superVal="sub_ch_super";
subObj.superVang="sub_superVang";
console.log('super.superVal => '+superObj.superVal); // prototype link 값을 변경시 !! 변화 없음
console.log('sub.superVal => '+subObj.superVal); // *prototype link 값을 변경시(아래결과로 prototype을 가르켜는게 아닌것으로 판단)  !! 자신에 객체가 값은 변하지만 super object 값은 변경되지 않는다.
console.log('subObj.superVang => '+subObj.superVang); // prototype link 값을 변경하는 것이 아니라 자신의 객채를 생성하여 값을 대입한다.


/*  */
console.log('');
console.log('=====객체 생성 상속=====');

var kim={
   name:'kim',
   first:10,second:20,
   sum:function(){return this.first+this.second}
}
console.log('kim.sum() => ',kim.sum());
// var lee={
//    name:'lee',
//    first:10,second:10,
//    avg:function(){return this.sum()/2}
// }
// lee.__proto__=kim; // proto link  하여 사용
var lee = Object.create(kim);
lee.name='lee';
lee.first=10;
lee.second=10;
lee.avg=function(){return this.sum()/2}
// console.log('lee.sum() => ', lee.sum()); // * proto link 전 에러 발생
console.log('lee.sum() => ', lee.sum()); // 사용가능 proto link 후
console.log('lee.avg() => ', lee.avg());

// Object.create   익스 9까지 지원   (이전 버전은 폴리필 을 사용하여 적용 가능 )
