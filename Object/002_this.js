var kim = {
  name:'kim',
  first:10,
  second:20,
  sum1:function(f, s){
    return f+s;
  },
  sum2:function(){
    return kim.first+kim.second;
  },
  sum3:function(){
    return this.first+this.second;
  },
}

console.log("kim.sum(kimfirst, kim.second)", kim.sum1(kim.first, kim.second));
console.log("kim.sum() Name : ", kim.sum2());
console.log("kim.sum() This : ", kim.sum3());

// this 정의  지금 내가 포함되어 있는 객체