/* 
    배열 깊은 복사 후에 원본은 변경없이 원하는 값이 담기 배열을 꺼내온다
    slice(start, end)
    sort((a,b) => a.num - b.num)  
    sort((a,b) => a.string > b.string ? -1 : a.string < b.string ? 1 : 0) 
*/

const array = new Array(
    {
        name: 'KKu',
        age:  34,
    },
    {
        name: 'Miso',
        age: 8,
    },
    {
        name: 'KKuisland',
        age: 28,
    }
)

console.log('first', array)

console.log('second', sortByArray(array))


function sortByArray(array) {
    const _array = array.slice() // array.slice(start, end)
    // _array.sort((a, b) => a.age - b.age ) // 오름 차순
    // _array.sort((a, b) => b.age - a.age ) // 내림 차순
    // _array.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0 ) // 오름 차순
    _array.sort((a, b) => a.name > b.name ? -1 : a.name < b.name ? 1 : 0 ) // 내림 차순
    return _array
}

