class Person{
   constructor(name,first,second){
      this.name=name;
      this.first=first;
      this.second=second;
   }
   sum(){
      return this.first+this.second;
   }
}

class PersonInheritance extends Person{ // 상속후 
   constructor(name, first, second, third){
      super(name, first, second);
      this.third=third;
   }
   names(param1=this.name, param2=this.first, param3=this.second){ // overload 구현
      if(arguments.length===3){
         return "매개변수1 : "+param1+"\t매개변수2 : "+param2+"\t매개변수3 : "+param3;
      }else if(arguments.length===2){
         return "매개변수1 : "+param1+"\t매개변수2 : "+param2;
      }else if(arguments.length===1){
         return "매개변수1 : "+param1;
      }else{
         return "매개변수 없음\t 기본값 \t"+"매개변수1 : "+param1+"\t매개변수2 : "+param2+"\t매개변수3 : "+param3;
      }
   }

   sum(){ // 오버라이딩
      return super.sum()+this.third;
   }
   avg(){
      return this.sum()/3;
   }
   div(){
      return this.first/this.second;
   }
}

let kim = new PersonInheritance('kim', 10, 20,50);
console.log(kim.names());
// console.log(kim.names());
console.log(kim.sum());
console.log(kim.avg());
console.log(kim.div());
