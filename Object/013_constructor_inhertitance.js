function Person(name,first,second){
   this.name=name;
   this.first=first;
   this.second=second;
}
Person.prototype.sum=function(){
   return this.first+this.second;
}
PersonPlus.prototype.avg=function(){
   return (this.first+this.second+this.third)/3;
}

// PersonPlus.prototype.__proto__=Person.prototype;
PersonPlus.prototype = Object.create(Person.prototype); // 공식 표현
PersonPlus.prototype.constructor=PersonPlus;

function PersonPlus(name,first,second,third){ // 위치가 위로 가면 안된다.  위로 갔을때에는  __proto__ 를 사용
   Person.call(this,name,first,second); // 생성자 기능을 해준다.
   this.third=third;
}



var kim=new PersonPlus('kim',10,20,30);
console.log('kim.sum()', kim.sum());
console.log('kim.avg()', kim.avg());
console.log('kim.constructor', kim.constructor);

