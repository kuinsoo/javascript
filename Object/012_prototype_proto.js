/* prototype vs __proto__ */

/*
   function is ? 

   == function Person(){}
   == var Person = new Function();

   객체
*/

function Person(name,first,second){
   this.name=name;
   this.first=first;
   this.second=second;
}

/* 
   Person            Person's prototype 
      - prototype       - constructor

   prototype      -->   Person's prototype
   constructor    -->   Person

*/

Person.prototype.sum=function(){}
/* 
   Person            Person's prototype 
      - prototype       - constructor
                        - sum

   prototype      -->   Person's prototype
   constructor    -->   Person

*/

var kim=new Person('kim',10,20);
/* 
   kim                     Person's prototype
      __proto__               constructor
      name                    sum
      first
      second

      __proto__         -->   Person's prototype
      Person.prototype  -->   Person's Prototype
*/

var lee=new Person('lee',10,10);
/* 
   lee                     Person's prototype
      __proto__               constructor
      name                    sum
      first
      second

      __proto__         -->   Person's prototype
      Person.prototype  -->   Person's Prototype
*/

console.log(kim.name);
/* 
  kim                
    __proto__       
    name *표현            
    first
    second
    __proto__       
    Person.prototype
*/

kim.sum();
/* proto에서 찾는다
   kim                     Person's prototype
      __proto__               constructor
      name                    sum
      first
      second

      __proto__         -->   Person's prototype   (in)->   sum()
*/